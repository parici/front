import { Component, OnInit} from '@angular/core';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { LoginComponent } from './components/login/login.component';
import { EditComponent } from './components/edit/edit.component';
import * as data from './database/stores.json';
import { Product, Store } from './interfaces/store.interface';
import { GraphqlService } from './graphql.service';
import { StoreReader } from 'apollo-cache-inmemory';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'ParIci';
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  allowEdit = false;
  username: string;
  password: string;
  storeList: Array<Store>;
  dataStores: Array<Store>;
  newStore: Store = new Store();
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  products: Product[] = [
  ];

  innerWidth: any;

  constructor(public dialog: MatDialog,
              private graphqlService: GraphqlService) {
  }

  // Table view
  // displayedColumns: string[] = ['id', 'name', 'post', 'prod'];
  // dataSource = new MatTableDataSource(STORES);
  /*loadData = data.stores as Array<Store>;

  dataStores = this.loadData.sort((a, b) => {
    if (!a.city) {
      a.city = '';
    }

    if (!b.city) {
      b.city = '';
    }
    const nameA = a.city.toUpperCase(); // ignora maiuscole e minuscole
    const nameB = b.city.toUpperCase(); // ignora maiuscole e minuscole
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }
    // i nomi devono essere uguali
    return 0;
  });*/

  ngOnInit() {
    // Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    // Add 'implements OnInit' to the class.

    const token = localStorage.getItem('token');
    if (token) {
      this.allowEdit = true;
    }

    this.graphqlService.getStores().subscribe(result => {
      this.storeList = result.StoreList;
      this._sortStoreList();
    });

    this.innerWidth = window.innerWidth;
  }

  openEdit(store: Store): void {
    const dialogRef = this.dialog.open(EditComponent, {
      minWidth: '300px',
      data: store
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 'delete') {
        this.graphqlService.deleteStore(store.id).subscribe((deleteMsg) => {
          this._updateStores();
        });
      } else if (result) {
        this.graphqlService.editStore(result.id, result).subscribe((newStore) => {
          this._updateStores();
        });
      }
    });
  }

  openNew(store: Store): void {
    const dialogRef = this.dialog.open(EditComponent, {
      minWidth: '300px',
      data: store
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.graphqlService.addStore(result).subscribe((newStore) => {
          this.storeList.push(newStore.data.AddStore);
          this._sortStoreList();
        });
      }
    });
  }

  openLogin(): void {
    // localStorage.removeItem('token');
    const dialogRef = this.dialog.open(LoginComponent, {
      minWidth: '300px',
      data: {username: this.username, password: this.password}
    });

    dialogRef.afterClosed().subscribe(result => {
      const token = localStorage.getItem('token');
      if (token) {
        this.allowEdit = true;
      }
      // this.password = result;
    });
  }



  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
      this.products.push({name: value.trim()});
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  remove(product: Product): void {
    const index = this.products.indexOf(product);

    if (index >= 0) {
      this.products.splice(index, 1);
    }
  }

  applyFilter(filterValue: string) {
    // this.dataSource.filter = filterValue.trim().toLowerCase();
    this.dataStores = this.storeList.filter(element => {

      let products = false;
      let name = false;
      let city = false;

      if (element.products) {
        products = element.products.toLocaleLowerCase().includes(filterValue.trim().toLowerCase());
      }

      if (element.name) {
        name = element.name.toLocaleLowerCase().includes(filterValue.trim().toLowerCase());
      }

      if (element.city) {
        city = element.city.toLocaleLowerCase().includes(filterValue.trim().toLowerCase());
      }

      return products || name || city;
    });
  }

  logout() {
    this.allowEdit = false;
    this.graphqlService.logout();
  }

  _updateStores() {
    this.graphqlService.getStores().subscribe(stores => {
      this.storeList = stores.StoreList;
      this._sortStoreList();
    });
  }

  _sortStoreList() {
    this.dataStores = this.storeList.sort((a, b) => {
      if (!a.city) {
        a.city = '';
      }

      if (!b.city) {
        b.city = '';
      }
      const nameA = a.city.toUpperCase(); // ignora maiuscole e minuscole
      const nameB = b.city.toUpperCase(); // ignora maiuscole e minuscole
      if (nameA < nameB) {
        return -1;
      }
      if (nameA > nameB) {
        return 1;
      }
      // i nomi devono essere uguali
      return 0;
    });
  }
}
