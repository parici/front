import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Store } from 'src/app/interfaces/store.interface';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  newTime: string;
  editStore: Store = new Store();

  constructor(
    public dialogRef: MatDialogRef<EditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Store) {
      this.editStore.id = data.id;
      this.editStore.name = data.name;
      this.editStore.city = data.city;
      this.editStore.address = data.address;
      this.editStore.products = data.products;
      if (data.time) {
        this.editStore.time = data.time;
      }
      this.editStore.website = data.website;
      this.editStore.phone = data.phone;
      // this.editStore = data;
     }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  addTime() {
    this.editStore.time.push(this.newTime);
    this.newTime = '';
  }

  deleteTime(index: number) {
    this.editStore.time.splice(index, 1);
  }

  save() {
    this.dialogRef.close(this.editStore);
  }

  delete() {
    this.dialogRef.close('delete');
  }

  cancelEdit() {
    this.dialogRef.close();
  }
}
