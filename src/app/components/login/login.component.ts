import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { GraphqlService } from 'src/app/graphql.service';

export interface DialogData {
  username: string;
  password: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;

  constructor(public dialogRef: MatDialogRef<LoginComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData,
              private graphqlService: GraphqlService) { }



  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  login(): void {
    // if (this.username === 'admin' && this.password === 'admin') {
    this.graphqlService.login(this.username, this.password).subscribe(response => {
      // Set received token to local store
      localStorage.setItem('token', response.Login.token);
      // this.data = { username: this.username, password: this.password};
      this.dialogRef.close(response);
    });
   // } else {
   //   alert('Invalid credentials');
   // }
  }

}
