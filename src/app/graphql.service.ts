import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Store } from './interfaces/store.interface';
import gql from 'graphql-tag';

export interface LoginData {
  token: string;
  user: string;
}

export interface AuthPayload {
  Login: LoginData;
}

export interface StoreList {
  StoreList: Array<Store>;
}

const loginQuery = gql`
query Login($user: String!, $password: String!) {
  Login(user:$user, password:$password) {
    token
    user
  }
}`;

const storesQuery = gql`
  query getStores {
  StoreList {
    id
    name
    city
    products
    address
    time
    website
  }
}`;

const addStoreMutation = gql`
  mutation addStore($store: Store_Input!) {
  AddStore(store: $store) {
    id
    name
    city
    address
    time
    phone
    products
    website
  }
}`;

const editStoreMutation = gql`
  mutation editStore($id: Int!, $store: Store_Input!) {
  EditStore(id: $id, store: $store) {
    id
    name
    city
    address
    time
    phone
    products
    website
  }
}`;

const deleteStoreMutation = gql`
  mutation DeleteStore($id: Int!) {
  DeleteStore(id: $id)
}`;


@Injectable({
  providedIn: 'root'
})
export class GraphqlService {

  constructor(private apollo: Apollo) { }

  logout() {
    localStorage.removeItem('token');
    this.apollo.getClient().resetStore();
  }

  login(user: string, password: string): Observable<AuthPayload> {
    return this.apollo.watchQuery<AuthPayload>({
      query: loginQuery,
      variables: {
        user,
        password
      }
    }).valueChanges.pipe( map(result => {
      // Return login data
      return result.data;
    }));
  }

  getStores(): Observable<StoreList> {
    return this.apollo.watchQuery<StoreList>({
      query: storesQuery
    }).valueChanges.pipe(map(result => {
      // Return store list
      return result.data;
    }));
  }

  addStore(store: Store): Observable<any> {
    return this.apollo.mutate<Store>({
      mutation: addStoreMutation,
      variables: {
        store
      }
    });
  }

  editStore(id: number, store: Store): Observable<any> {
    return this.apollo.mutate<Store>({
      mutation: editStoreMutation,
      variables: {
        id,
        store
      }
    });
  }

  deleteStore(id: number): Observable<any> {
    return this.apollo.mutate<Store>({
      mutation: deleteStoreMutation,
      variables: {
        id
      }
    });
  }
}
