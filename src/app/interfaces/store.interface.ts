export interface Product {
    name: string;
}


export class Store {
    id: number;
    name: string;
    city: string;
    address: string;
    phone: string;
    time: Array<string>;
    website: string;
    products: string;

    constructor() {
        this.id = 0;
        this.time = new Array<string>();
    }
}
