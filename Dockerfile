### STAGE 1: Build ###

# We label our stage as 'builder'
FROM node:8-alpine as builder

# ENV http_proxy http://cerberus.rsvgnw.local:8080
# ENV https_proxy http://cerberus.rsvgnw.local:8080

RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh

COPY package*.json ./

RUN npm set progress=false && npm config set depth 0 && npm cache clean --force

## Storing node modules on a separate layer will prevent unnecessary npm installs at each build
RUN npm i && mkdir /ng-app && cp -R ./node_modules ./ng-app

WORKDIR /ng-app

COPY . .

## Build the angular app in production mode and store the artifacts in dist folder
RUN $(npm bin)/ng build --prod --build-optimizer

### STAGE 2: Setup ###

FROM centos/nginx-112-centos7

# ENV http_proxy http://cerberus.rsvgnw.local:8080
# ENV https_proxy http://cerberus.rsvgnw.local:8080

## Install bash to run docker exec on bash
#RUN apk update && apk upgrade && \
#    apk add --no-cache bash

## Copy our default nginx config
COPY nginx/default.conf /etc/nginx/conf.d/

## Remove default nginx website
RUN rm -rf /usr/share/nginx/html/*

## From 'builder' stage copy over the artifacts in dist folder to default nginx public folder
COPY --from=builder /ng-app/dist/StoreDark /usr/share/nginx/html

## Copy the EntryPoint
#COPY ./entryPoint.sh /

# Correct file format to linux standard
#RUN sed -i -e 's/\r$//' /entryPoint.sh
## Edit file rights to run correctly at startup 
#RUN chmod +x entryPoint.sh

#ENTRYPOINT ["/entryPoint.sh"]
CMD ["nginx", "-g", "daemon off;"]